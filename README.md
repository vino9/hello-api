# Hello API

A very simple json api written in golang which includes an example pipeline and builds into a container.


# Build the binary

```
GOOS=linux GOARCH=amd64 go build -o ./Docker/helloapi .
```

CD into Docker and build the container

```
cd Docker/
docker build .
docker push registry.gitlab.com/theuberlab/hello-api
```


Run the container exposing port 8443
```
docker run -p 8443:8443 <image ID>
```

Or just run the one I published

```
docker run -p 8443:8443 registry.gitlab.com/theuberlab/hello-api
```

Curl the /message endpoint

```
[x4e5@M-C02TN25DGTF1:hello-api (initial):]
$ curl -k https://localhost:8443/message
{"Message":"Hello","User":"World"}
```

```
[thesporkboy:hello-api (initial):]
$ curl -k https://localhost:8443/message/bob
{"Message":"Hello","User":"bob"}
```